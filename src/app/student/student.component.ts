import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
selector: 'app-student',
templateUrl: './student.component.html',
styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
    title = 'TemplateFormDemo';

    ngOnInit(): void {

    }
    // tslint:disable-next-line:typedef
    onClickSubmit(formData: { name: string; addmissionnum: any; password: string; feepaid: string; feepending: string }) {
    alert('student name is : ' + formData.name);
    alert('student addmissionnum is : ' + formData.addmissionnum);
    alert( 'student password is: ' + formData.password);
    alert( 'student feepaid is:' + formData.feepaid);
    alert('student feepending is: ' + formData.feepending);
    }
}