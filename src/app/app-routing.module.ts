import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { GalleryComponent } from './gallery/gallery.component';
import { StudentComponent } from './student/student.component';
import {HomeComponent } from './home/home.component';

const routes: Routes = [{path: 'aboutus', component: AboutusComponent},
{path: 'contactus', component: ContactusComponent},
{path: 'gallery', component: GalleryComponent},
{path: 'student', component: StudentComponent},
{path: 'home', component: HomeComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
